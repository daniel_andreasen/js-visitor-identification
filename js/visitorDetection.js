
/**
     *
    * Duck-typing checking of browser-type, then version-check via useragent
    * https://en.wikipedia.org/wiki/Duck_typing
    * Inspiration from
    * - https://jsfiddle.net/311aLtkz/ 
    * - https://gist.github.com/gaboratorium/25f08b76eb82b1e7b91b01a0448f8b1d 
    *
    * Different IE versions are described differently in the User Agent
    *  - IE 10 or older: MSIE [version number]
    *  - IE 11: Trident/[version number]
    *  - EDGE: Edge/[version number]
    *
*/  

function visitorInfo(){

    var ua = window.navigator.userAgent;  
    
    var userAgentInf = {
        Chrome: {
            InUse: !!window.chrome && !!window.chrome.webstore,
            Agent: "Chrome/",
            HumanName: "Google Chrome"
        },
        Opera: {
            InUse: (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0, // Opera 8.0+ 
            Agent: " OPR/",
            HumanName: "Opera"
        },
        Firefox: {
            InUse: typeof InstallTrigger !== 'undefined', // Firefox 1.0+
            Agent: "Firefox/",
            HumanName: "Firefox"
        },
        Safari: {
            InUse: /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification), // Safari 3.0+ "[object HTMLElementConstructor]" 
            Agent: "Version/",
            HumanName: "Safari"
        },
        IE: {
            InUse: /*@cc_on!@*/false || !!document.documentMode, // Internet Explorer 6-11
            Agent: {
                MSIE: "MSIE ",
                Trident: "Trident/",
            },
            HumanName: "Internet Explorer"
        },
        Edge: {
            InUse: !(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia, // Edge 20+
            Agent: "Edge/",
            HumanName: "Microsoft Edge"
        }
        ,
        
        Other: {
            InUse: false,
            Agent: "Unknown",
            HumanName: "Unknown"
        }
    }

    var visitorInfo = {
        
        OS: "",
        BrowserName: "",
        BrowserVersion: "",
        BrowserUserAgent: "",
        
        // Maybe not needed
        BrowserNote: "",
        
        // Set complete User Agent - For debugging purposes mainly
        UserAgentComplete: ua
    }

    // "Iterate" the browsertypes in the userAgentInf object
    for (var brw in userAgentInf){
        
        // Check if the .InUse attribute is set. If(yes) { perfom deeper analysis }
        if(userAgentInf[brw].InUse){

            // Get visitorinfo OS
            visitorInfo.OS = ua.substring(ua.indexOf("(") + 1, ua.indexOf(")"));
            
            // Get visitorinfo.browser "Human name"
            visitorInfo.BrowserName = userAgentInf[brw].HumanName;
            
            // Check for IE
            if(visitorInfo.BrowserName === "Internet Explorer"){
                
                // First, check for older versions using the MSIE version
                var msie = ua.indexOf(userAgentInf[brw].Agent.MSIE);
                if (msie > 0) {
                    visitorInfo.BrowserUserAgent = userAgentInf[brw].Agent.MSIE;
                    // IE 10 or older => return version number
                    visitorInfo.BrowserVersion = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                }

                // If not the older versions, the check for newer Trident versions
                else if (ua.indexOf(userAgentInf[brw].Agent.Trident) > 0) {
                    visitorInfo.BrowserUserAgent = userAgentInf[brw].Agent.Trident;
                    
                    // IE 11 => return version number
                    var rv = ua.indexOf('rv:');
                    visitorInfo.BrowserVersion = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                }
            
            }

            // If not IE
            else{

                // Get the browserspecific UserAgent. E.g.: "Chrome/" or "Trident/" etc.
                visitorInfo.BrowserUserAgent = userAgentInf[brw].Agent;

                // Index of User Agent string
                var uaPosition = ua.indexOf(visitorInfo.BrowserUserAgent)              
            
                // Set the substring[end] integer as either the index of the first [space] after User Agent start, or if no space can be found thereafter, then set it as the .length of the User Agent
                var browserVersionEnd = (uaPosition === ua.indexOf(' ', uaPosition) || ua.indexOf(' ', uaPosition) === -1 ? ua.length : ua.indexOf(' ', uaPosition))

                // Get actual browser version
                visitorInfo.BrowserVersion = ua.substring(uaPosition + userAgentInf[brw].Agent.length, browserVersionEnd);
            }
            

            //debugit();

            // Idea: check if BLINK engine (Chrome and Opera)
            /*
            if(!!window.CSS && [if chrome || oprea]){
                visitorInfo.Note = "With Blink engine";
            }
            */
        }
        // Else { do nothing  - because, who cares? }
    }

    return visitorInfo;

}


